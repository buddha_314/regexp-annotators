package com.qurius.uima.annotators;

import com.qurius.uima.annotations.RegExpMatch;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.OperationalProperties;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by buddha on 11/4/15.
 */
@ResourceMetaData(
        name = "Regular Expression Annotator",
        description = "This Annotator grabs a few common patterns using regular expressions.  See the README for more.",
        vendor = "Qurius",
        version = "1.0",
        copyright = "ASF"
)
@OperationalProperties(
        modifiesCas = true,
        outputsNewCases = false
)
@TypeCapability(outputs = {"com.qurius.uima.annotations.RegExpMatch"})
public class Regexps extends JCasAnnotator_ImplBase {
    private Pattern zipCode;
    private Pattern ip4;


    public void initialize(UimaContext aContext) throws ResourceInitializationException {
        super.initialize(aContext);
        zipCode = Pattern.compile("\\s+[0-9]{5}(?:-[0-9]{4})?");
        ip4 = Pattern.compile("(?:[0-9]{1,3}\\.){3}[0-9]{1,3}");
    }

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        String content = aJCas.getDocumentText();

        // Zip codes...
        Matcher zipMatcher = zipCode.matcher(content);
        addMatch(aJCas, zipMatcher, "zipcode");

        // IP v4
        Matcher ip4Matcher = ip4.matcher(content);
        addMatch(aJCas, ip4Matcher, "ip4");
    }

    private void addMatch(JCas aJCas, Matcher m, String n) {
       while(m.find()) {
           RegExpMatch annotation = new RegExpMatch(aJCas);
           annotation.setPattern(n);
           annotation.setBegin(m.start());
           annotation.setEnd(m.end());
           annotation.addToIndexes();
       }
    }
}
